﻿using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Alumno : Entity
    {
        public string Nombre { get; set; }
        public Alumno(string nombre)
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
        }
    }
}