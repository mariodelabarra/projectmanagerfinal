﻿using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Dominio.SeedWoork;
using EscuelaNet.Presentacion.Programadores.Web.Infraestructura;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Programadores.Repositorios
{
    public class EquipoSingletonRepository : IEquipoRepository
    {
        private EquipoSingleton contexto = EquipoSingleton.Instancia;
        public IUnitOfWork UnitOfWork => contexto;

        public Equipo Add(Equipo equipo)
        {
            contexto.Equipos.Add (equipo);
            return equipo;
        }

        public void Delete(Equipo equipo)
        {
            contexto.Equipos.Remove(equipo);
        }

        public void DeleteProgramador(Programador programador)
        {
            throw new NotImplementedException();
        }

        public Equipo GetEquipo(int id)
        {
            return contexto.Equipos[id];
        }

        public List<Equipo> ListEquipo()
        {
            return contexto.Equipos.ToList();
        }

        public void Update(Equipo equipo)
        {
            contexto.Equipos.RemoveAt (equipo.ID);
            contexto.Equipos.Insert (equipo.ID, equipo);
        }
    }
}
