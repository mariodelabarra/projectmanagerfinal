CREATE TABLE [dbo].[Capacitaciones]
(
   [IDCapacitacion] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
  [IDLugar] INT NOT NULL,
  [Minimo] INT NOT NULL,
  [Maximo] INT NOT NULL,
  [Duracion] INT NOT NULL,
  [Precio] DECIMAL NOT NULL,
  [Estado] INT NOT NULL,
  [Inicio] DATETIME NOT NULL,
  [Fin] DATETIME  NOT NULL
)