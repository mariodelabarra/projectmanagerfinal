﻿using System.Data.Entity.ModelConfiguration;
using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Infraestructura.Capacitaciones
{
    public class InstructoresEntityTypeConfiguration : EntityTypeConfiguration<Instructor>
    {
        public InstructoresEntityTypeConfiguration()
        {
            this.ToTable("Instructores");
            this.HasKey<int>(i => i.ID);
            this.Property(i => i.ID)
                .HasColumnName("IDInstructor");
            this.Property(c => c.Nombre)
                .IsRequired();
            this.Property(c => c.Apellido)
                .IsRequired();
            this.Property(c => c.Dni)
                .IsRequired();
            this.Property(c => c.FechaNacimiento)
                .IsRequired();
            this.HasMany<Tema>(i=>i.Temas)
                .WithMany(t=>t.Instructores)
                .Map(it => {
                    it.MapLeftKey("IDInstructor");
                    it.MapRightKey("IDTema");
                    it.ToTable("InstructorTema");
                });
        }
    }
}