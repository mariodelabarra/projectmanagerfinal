﻿USE [Proyectos]
GO

/****** Object:  Table [dbo].[EtapaTecnologia]    Script Date: 18/9/2019 22:00:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EtapaTecnologia](
	[IDEtapa] [int] NOT NULL,
	[IDTecnologia] [int] NOT NULL,
 CONSTRAINT [PK_EtapaTecnologia] PRIMARY KEY CLUSTERED 
(
	[IDEtapa] ASC,
	[IDTecnologia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO