﻿using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Dominio.SeedWoork;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Proyectos.Repositorios
{
    public class TecnologiaRepository : ITecnologiaRepository
    {
        private LineaContext _contexto;
        public TecnologiaRepository(LineaContext contexto)
        {
            _contexto = contexto;
        }
        public IUnitOfWork UnitOfWork => _contexto;
        

        public Tecnologias Add(Tecnologias tecnologias)
        {
            _contexto.Tecnologias.Add(tecnologias);
            return tecnologias;
        }

        public void Delete(Tecnologias tecnologias)
        {
            _contexto.Tecnologias.Remove(tecnologias);
        }
        public Tecnologias GetTecnologia(int id)
        {
            var tecnologia = _contexto.Tecnologias.Find(id);
            if (tecnologia != null)
            {
                _contexto.Entry(tecnologia);
            }
            return tecnologia;
        }
 
        public List<Tecnologias> ListTecnologias()
        {
            return _contexto.Tecnologias.ToList();
        }

        public void Update(Tecnologias tecnologia)
        {
            _contexto.Entry(tecnologia).State = EntityState.Modified;
        }
    }
}

