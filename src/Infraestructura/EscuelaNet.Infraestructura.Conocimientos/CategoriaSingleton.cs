﻿using EscuelaNet.Dominio.Conocimientos;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Conocimientos
{
    public sealed class CategoriaSingleton : IUnitOfWork
    {
        private static CategoriaSingleton _instancia = new CategoriaSingleton();
        public List<Categoria> Categorias { get; set; }

        private CategoriaSingleton()
        {
            this.Categorias = new List<Categoria>();
            Categorias.Add(new Categoria("Programacion", "POO"));

            var asesor = new Asesor("milagros", "Gomez aranda", "ingles", "argentina");
            var conocimiento = new Conocimiento(".net");
            asesor.pushConocimiento(conocimiento);

            var asesor1 = new Asesor("oscar", "maluff", "aleman", "uruguay");
            var conocimiento1 = new Conocimiento("Java");

            var conocimientoCategoria = new Conocimiento("C#");
            this.Categorias[0].AgregarConocimiento(conocimientoCategoria);
            this.Categorias[0].AgregarAsesor(asesor);
            this.Categorias[0].AgregarAsesor(asesor1);
        }

        

        public static CategoriaSingleton Instancia
        {
            get
            {
                return _instancia;
            }
        }

        public int SaveChanges()
        {
            return 1;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

    }
}
