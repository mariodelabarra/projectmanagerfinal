﻿using EscuelaNet.Dominio.Conocimientos;
using EscuelaNet.Dominio.SeedWoork;
using EscuelaNet.Infraestructura.Conocimientos.EntityTypeConfigurations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Conocimientos
{
    public class CategoriaContext : DbContext, IUnitOfWork
    {
        public CategoriaContext() : base("ConocimintosContext")
        {
        }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Conocimiento> Conocimientos { get; set; }
        public DbSet<Asesor> Asesores { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CatagoriaEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new ConocimientosEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new AsesorEntityTypeConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
