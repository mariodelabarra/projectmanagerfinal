﻿using EscuelaNet.Dominio.Clientes;
using System.Data.Entity.ModelConfiguration;

namespace EsculaNet.Infraestructura.Clientes.EntityTypeConfigurations
{
    public class DireccionesEntityTypeConfiguration :
        EntityTypeConfiguration<Direccion>
    {
        public DireccionesEntityTypeConfiguration()
        {
            this.ToTable("Direcciones");
            this.HasKey<int>(d => d.ID);
            this.Property(d => d.ID)
                .HasColumnName("IDDireccion");
            this.Property(d => d.Domicilio)                
                .IsRequired();
            this.Property(d => d.Localidad)
                .IsRequired();
            this.Property(d => d.Provincia)
               .IsRequired();
            this.Property(d => d.Pais)
                .IsRequired();           
            this.Property(d => d.IDUnidadDeNegocio)
                .IsRequired();
        }
    }
}