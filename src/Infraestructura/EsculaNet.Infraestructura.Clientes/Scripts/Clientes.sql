﻿CREATE TABLE [dbo].[Clientes]
(
	[IDCliente] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [RazonSocial] VARCHAR(128) NOT NULL, 
    [Email] VARCHAR(255) NOT NULL, 
    [Categoria] INT NOT NULL
)
