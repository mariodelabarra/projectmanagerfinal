﻿using EscuelaNet.Presentacion.Proyectos.Web.Infraestructura;
using EscuelaNet.Presentacion.Proyectos.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Proyectos.Web.Controllers
{
    public class LineaDeProduccionController : Controller
    {
        // GET: LineaDeProduccion
        public ActionResult Index()
        {
            var lineasDeProduccion = 
                Contexto.Instancia.LineasDeProduccion;
            var model = new LineasIndexModel()
            {
                Titulo = "Primera Prueba"
            };

            return View(model);
        }

        public ActionResult New()
        {
            return View;
        }
        
    }
}