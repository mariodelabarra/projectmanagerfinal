﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EscuelaNet.Dominio.Proyectos;

namespace EscuelaNet.Presentacion.Proyectos.Web.Models
{
    public class EtapaIndexModel
    {
        public string Nombre { get; set; }
        public List<Etapa> Etapas { get; set; }
        public int IDLinea { get; set; }
        public int IDProy { get;  set; }
        
    }
}