﻿using EscuelaNet.Aplicacion.Proyectos.Commands.LineaCommand;
using EscuelaNet.Aplicacion.Proyectos.QueryServices.LineaServices;
using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Infraestructura.Proyectos;
using EscuelaNet.Infraestructura.Proyectos.Repositorios;
//using EscuelaNet.Presentacion.Proyectos.Web.Infraestructura;
using EscuelaNet.Presentacion.Proyectos.Web.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace EscuelaNet.Presentacion.Proyectos.Web.Controllers
{
    public class LineasController : Controller
    {
        private ILineaRepository _lineaRepository;
        private ILineaQuery _lineaQuery;
        private IMediator _mediator;
        public LineasController(ILineaRepository lineaRepository,
            ILineaQuery lineaQuery, IMediator mediator)
        {
            _lineaRepository = lineaRepository;
            _lineaQuery = lineaQuery;
            _mediator = mediator;
        }
        // GET: Default
        public ActionResult Index()
        {
            var lineasDeProduccion = _lineaQuery.ListLinea();
            var model = new LineasIndexModel()
            {
                Titulo = "Linea de Produccion",
                LineasDeProduccion = lineasDeProduccion,
            };
            return View(model);
        }

        public ActionResult New()
        {
            var model = new NuevaLineaModel();
            return View(model);

        }
        [HttpPost]
        public async Task<ActionResult> New(NuevaLineaCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Success)
            {
                TempData["success"] = "Linea de Producción creada";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaLineaModel()
                {
                    Nombre = model.Nombre
                };
                return View(modelReturn);
            }
        }

        public ActionResult Edit(int id)
        {
            var linea = _lineaQuery.GetLinea(id);
            var model = new NuevaLineaModel() {
                Nombre = linea.Nombre,
                Id = id
            };
            return View(model);

        }
        [HttpPost]
        public ActionResult Edit(NuevaLineaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var linea = _lineaRepository.GetLinea(model.Id);
                    linea.Editar(model.Nombre);
                    _lineaRepository.Update(linea);
                    _lineaRepository.UnitOfWork.SaveChanges();
                    TempData["success"] = "Linea de producción editada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }
        public ActionResult Delete(int id)
        {
            var linea = _lineaRepository.GetLinea(id);
            var model = new NuevaLineaModel()
            {
                Nombre = linea.Nombre,
                Id = id
            };
            return View(model);

        }
        [HttpPost]
        public ActionResult Delete(NuevaLineaModel model)
        {
           
                try
                {
                    var linea = _lineaRepository.GetLinea(model.Id);
                    _lineaRepository.Delete(linea);
                    _lineaRepository.UnitOfWork.SaveChanges();
                    TempData["success"] = "Linea de producción borrada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

           

        }


    }
}