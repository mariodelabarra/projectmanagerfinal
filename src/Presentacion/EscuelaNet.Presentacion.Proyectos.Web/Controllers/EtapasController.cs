﻿using EscuelaNet.Presentacion.Proyectos.Web.Infraestructura;
using EscuelaNet.Presentacion.Proyectos.Web.Models;
using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Proyectos.Web.Controllers
{
    public class EtapasController : Controller
    {
        private ILineaRepository _lineaRepository;
        public ITecnologiaRepository _tecnoRepository { get; set; }
        public EtapasController(ILineaRepository lineaRepository, ITecnologiaRepository tecnologiaRepository)
        {
            _lineaRepository = lineaRepository;
            _tecnoRepository = tecnologiaRepository;
        }
        // GET: Etapa
        public ActionResult Index(int idLinea, int idProy)
        {

            var proyecto = _lineaRepository.GetProyecto(idProy);
            if(proyecto.Etapas == null)
            {
                TempData["error"] = "Proyecto sin etapas";
                return RedirectToAction("../Proyectos/Index/?id=" + idLinea);
            }
            var model = new EtapaIndexModel() {
                Etapas = proyecto.Etapas.ToList(),
                IDLinea = idLinea,
                IDProy = idProy
            };

            return View(model);

        }

        public ActionResult New(int id)
        {
            var proyecto = _lineaRepository.GetProyecto(id);
            var model = new NuevaEtapaModel() { IDProy = id };
            return View(model);
        }

        [HttpPost]

        public ActionResult New(NuevaEtapaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var proyecto = _lineaRepository.GetProyecto(model.IDProy);

                    proyecto.PushEtapa(model.Nombre, model.duracion);
                    _lineaRepository.Update(proyecto.LineaDeProduccion);
                    _lineaRepository.UnitOfWork.SaveChanges();
                    TempData["success"] = "La etapa se ha creado";
                    return RedirectToAction("Index", new { idLinea = model.IDLinea, idProy = model.IDProy });
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            } else
            {
                TempData["error"] = "Debe completar ambos campos ";
                return View(model);
            }
        }

        public ActionResult Edit(int idProy, int idLinea, int idEtapa)
        {
            var etapas = _lineaRepository.GetEtapa(idEtapa);
            var proyecto = _lineaRepository.GetProyecto(etapas.IDProyecto);

            var model = new NuevaEtapaModel()
            {
                Nombre = etapas.Nombre,
                ID = idEtapa,
                duracion = etapas.Duracion,
                IDLinea = idLinea,
                IDProy = idProy
            };
            return View(model);
        }

        [HttpPost]

        public ActionResult Edit(NuevaEtapaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var proyecto = _lineaRepository.GetProyecto(model.IDProy);
                    proyecto.Etapas.Where(e => e.ID == model.ID).First().Nombre = model.Nombre;
                    proyecto.Etapas.Where(e => e.ID == model.ID).First().Duracion = model.duracion;

                    _lineaRepository.Update(proyecto.LineaDeProduccion);
                    _lineaRepository.UnitOfWork.SaveChanges();
                    TempData["success"] = "Etapa modificada";
                    return RedirectToAction("index", new { idLinea=model.IDLinea,idProy=model.IDProy }); 
                }
                catch(Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Debe completar ambos campos";
                return View(model);
            }
        }

        public ActionResult Delete(int idLinea, int idProy, int idEtapa)
        {
            var etapa = _lineaRepository.GetEtapa(idEtapa);
            var proyecto = _lineaRepository.GetProyecto(etapa.IDProyecto);
            var model = new NuevaEtapaModel()
            {
                Nombre = etapa.Nombre,
                ID = idEtapa
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(NuevaEtapaModel model)
        {
            try
            {
                var etapa = _lineaRepository.GetEtapa(model.ID);
                _lineaRepository.DeleteEtapa(etapa);
                _lineaRepository.UnitOfWork.SaveChanges();
                TempData["success"] = "Etapa eliminada";
                return RedirectToAction("Index", new { idLinea=model.IDLinea, idProy = model.IDProy });
            }
            catch(Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

        public ActionResult Tecnologias(int idLinea, int idEtapa)
        {
            var etapa = _lineaRepository.GetEtapa(idEtapa);
            var model = new EtapaTecnologiaModel()
            {
                Etapa = etapa,
                Tecnologias = etapa.Tecnologias.ToList(),
                IDLinea = idLinea
            };
            return View(model);

        }

        public ActionResult NewTecno(int idLinea, int idProy, int idEtapa)
        {
            var etapa = _lineaRepository.GetEtapa(idEtapa);
            var tecno = _tecnoRepository.ListTecnologias();
            var model = new NuevaEtapaTecnologiaModel()
            {
                IDLinea = idLinea,
                IDProy = idProy,
                IDEtapa = idEtapa,
                NombreTecnologia = etapa.Nombre,
                Tecnologias = tecno
            };
            return View(model);
        }

        [HttpPost]

        public ActionResult NewTecno(NuevaEtapaTecnologiaModel model)
        {
            try
            {
                var lineaBuscada = _lineaRepository.GetLinea(model.IDLinea);
                var etapaBuscada = _lineaRepository.GetEtapa(model.IDEtapa);
                var tecnoBuscada = _tecnoRepository.GetTecnologia(model.IDTecno);
                etapaBuscada.pushTecnologia(tecnoBuscada);
                _lineaRepository.Update(lineaBuscada);
                _lineaRepository.UnitOfWork.SaveChanges();

                TempData["success"] = "Tecnologia agregada";
                return RedirectToAction("Tecnologias", new { idLinea= model.IDLinea, idProy = model.IDProy, idEtapa = model.IDEtapa});
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

        public ActionResult DeleteTecno(int idLinea, int idTecno, int idEtapa)
        {
            var etapaBuscada = _lineaRepository.GetEtapa(idEtapa);
            var tecnoBuscada = _tecnoRepository.GetTecnologia(idTecno);

            var model = new NuevaEtapaTecnologiaModel()
            {
                NombreTecnologia = tecnoBuscada.nombre,
                IDEtapa = idEtapa,
                IDProy = etapaBuscada.IDProyecto,
                IDLinea = idLinea
            };
            return View(model);
        }

        [HttpPost]

        public ActionResult DeleteTecno(NuevaEtapaTecnologiaModel model)
        {
            try
            {
                var lineaBuscada = _lineaRepository.GetLinea(model.IDLinea);
                var etapaBuscada = _lineaRepository.GetEtapa(model.IDEtapa);
                var tecnoBuscada = etapaBuscada.Tecnologias.First(t => t.ID == model.IDTecno);
                etapaBuscada.pullTecnologia(tecnoBuscada);
                _lineaRepository.Update(lineaBuscada);
                _lineaRepository.UnitOfWork.SaveChanges();

                TempData["success"] = "Tecnologia borrada";
                return RedirectToAction("Tecnologias", new { idLinea= model.IDLinea, idEtapa= model.IDEtapa});
            }catch(Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
    }
}