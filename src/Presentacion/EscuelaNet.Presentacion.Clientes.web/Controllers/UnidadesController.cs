﻿using EscuelaNet.Aplicacion.Clientes.Commands.UnidadCommand;
using EscuelaNet.Aplicacion.Clientes.QueryServices;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using MediatR;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class UnidadesController : Controller
    {        
        private IMediator _mediator;
        private IClientesQuery _clientesQuery;
        private IUnidadesQuery _unidadesQuery;
        private ISolicitudesQuery _solicitudesQuery;

        public UnidadesController(
            IClientesQuery clientesQuery,
            IUnidadesQuery unidadesQuery,
            ISolicitudesQuery solicitudesQuery,
            IMediator mediator)
        {            
            _mediator = mediator;
            _unidadesQuery = unidadesQuery;
            _clientesQuery = clientesQuery;
            _solicitudesQuery = solicitudesQuery; 
        }

        public ActionResult Index(int id)
        {            
            var unidades = _unidadesQuery.ListUnidades(id);
            if (unidades.Count()!=0)
            {
                var model = new UnidadesIndexModel()
                {
                    Titulo = "Unidades del Cliente '" + unidades[0].RazonSocialCliente + "'",
                    Unidades = unidades,
                    IdCliente = id
                };

                return View(model);
            }
            else
            {
                TempData["error"] = "Cliente sin unidades";
                return RedirectToAction("../Clientes/Index");
            }

        }

        public ActionResult New(int id)
        {
            var cliente = _clientesQuery.GetCliente(id);
            var model = new NuevaUnidadModel()
            {
                Titulo = "Nueva unidad para el Cliente '"+cliente.RazonSocial+"'",
                IdCliente = id
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> New(NuevaUnidadCommand model)
        {

            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "Unidad creada";
                return RedirectToAction("Index/" + model.IdCliente);
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaUnidadModel()
                {
                    RazonSocial = model.RazonSocial,
                    ResponsableDeUnidad=model.ResponsableDeUnidad,
                    Cuit=model.Cuit,
                    EmailResponsable = model.EmailResponsable,
                    TelefonoResponsable=model.TelefonoResponsable,
                    IdCliente=model.IdCliente,
                };
                return View(modelReturn);
            }            
        }
        
        public ActionResult Edit(int id)
        {
            var unidad = _unidadesQuery.GetUnidadDeNegocio(id);
            if (unidad==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("../Clientes/Index");
            }
            else
            {                       
                var model = new NuevaUnidadModel()
                {
                    Titulo="Editar la Unidad '"+unidad.RazonSocial+"' del Cliente '"+ unidad.RazonSocialCliente+"'",
                    IdCliente = unidad.IdCliente,
                    IdUnidad = id,
                    RazonSocial = unidad.RazonSocial,
                    ResponsableDeUnidad = unidad.ResponsableDeUnidad,
                    EmailResponsable = unidad.EmailResponsable,
                    Cuit = unidad.Cuit,
                    TelefonoResponsable = unidad.TelefonoResponsable,                                       
                };
                return View(model);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditUnidadCommand model)
        {

            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "Unidad Editada";
                return RedirectToAction("Index/" + model.IdCliente);
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaUnidadModel()
                {
                    RazonSocial = model.RazonSocial,
                    ResponsableDeUnidad = model.ResponsableDeUnidad,
                    Cuit = model.Cuit,
                    EmailResponsable = model.EmailResponsable,
                    TelefonoResponsable = model.TelefonoResponsable,
                    IdCliente = model.IdCliente,
                    IdUnidad =model.IdUnidad
                };
                return View(modelReturn);
            }            
        }

        public ActionResult Delete(int id)
        {
            var unidad = _unidadesQuery.GetUnidadDeNegocio(id);
            if (unidad==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("../Clientes/Index");
            }
            else
            {                
                var model = new NuevaUnidadModel()
                {
                    Titulo = "Borrar la Unidad '" + unidad.RazonSocial + "' del Cliente '" + unidad.RazonSocialCliente + "'",
                    IdCliente = unidad.IdCliente,
                    IdUnidad = id,
                    RazonSocial = unidad.RazonSocial,
                    ResponsableDeUnidad = unidad.ResponsableDeUnidad,
                    EmailResponsable = unidad.EmailResponsable,
                    Cuit = unidad.Cuit,
                    TelefonoResponsable = unidad.TelefonoResponsable,
                };

                return View(model);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Delete(DeleteUnidadCommand model)
        {
            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "Unidad Borrada";
                return RedirectToAction("Index/" + model.IdCliente);
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaUnidadModel()
                {
                    RazonSocial = model.RazonSocial,
                    ResponsableDeUnidad = model.ResponsableDeUnidad,
                    Cuit = model.Cuit,
                    EmailResponsable = model.EmailResponsable,
                    TelefonoResponsable = model.TelefonoResponsable,
                    IdCliente = model.IdCliente,
                    IdUnidad = model.IdUnidad
                };
                return View(modelReturn);
            }
        }

        public ActionResult Solicitudes(int id)
        {
            var unidad = _unidadesQuery.GetUnidadDeNegocio(id);
            var solicitudes = _unidadesQuery.ListSolicitudesDeUnidad(id);
            var model = new UnidadSolicitudModel()
            {
                Unidad = unidad,
                Solicitudes = solicitudes
            };
            return View(model);
        }
        
        public ActionResult LinkSolicitud(int id)
        {
            var unidad = _unidadesQuery.GetUnidadDeNegocio(id);
            var solicitudes = _solicitudesQuery.ListSolicitud();

            var model = new NuevaUnidadSolicitudModel()
            {
                IDUnidad = id,
                RazonSocialUnidad = unidad.RazonSocial,
                Solicitudes = solicitudes
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> LinkSolicitud(LinkSolicitudCommand model)
        {
            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "Solicitud Vinculada";
                return RedirectToAction("../Unidades/Solicitudes/" + model.IDUnidad);
            }
            else
            {
                TempData["error"] = exito.Error;
                return RedirectToAction("../Unidades/Solicitudes/" + model.IDUnidad);
            }                  

        }

        public ActionResult UnlinkSolicitud(int id, int unidad)
        {
            var unidadBuscada = _unidadesQuery.GetUnidadDeNegocio(unidad);
            var solicitudBuscada = _unidadesQuery.FindSolicitud(id, unidad);

            if (unidadBuscada != null && solicitudBuscada != null)
            {
                var model = new NuevaUnidadSolicitudModel()
                {
                    IDSolicitud = id,
                    IDUnidad = unidad,
                    TituloSolicitud = solicitudBuscada.Titulo,
                    RazonSocialUnidad = unidadBuscada.RazonSocial
                };
                return View(model);
            }
            else
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("../Clientes/index");
            }
        }

        [HttpPost]
        public async Task<ActionResult> UnlinkSolicitud(UnlinkSolicitudCommand model)
        {
            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "Solicitud Desvinculada";
                return RedirectToAction("../Unidades/Solicitudes/" + model.IDUnidad);
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaUnidadSolicitudModel()
                {
                    IDSolicitud = model.IDSolicitud,
                    IDUnidad = model.IDUnidad,
                    TituloSolicitud = model.TituloSolicitud,
                    RazonSocialUnidad = model.RazonSocialUnidad
                };
                return View(modelReturn);
            }
        }

    }
}