﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Models
{
    public class NuevoAsesorConocimientoModel
    {
        public int IDAsesor { get; set; }
        public int IDConocimiento { get; set; }
        public string NombreConocimiento { get; set; }
        public string NombreAsesor { get; set; }
        public List<Asesor>Asesores { get; set; }
        public List<Conocimiento> Conocimientos { get; set; }
    }
}