﻿using EscuelaNet.Dominio.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Clientes.Web.Models
{
    public class NuevaDireccionModel
    {
        public string Titulo { get; set; }

        public int IdCliente { get; set; }

        public int IdUnidad { get; set; }

        public int IdDireccion { get; set; }

        public string Domicilio { get; set; }

        public string Localidad { get; set; }

        public string Provincia { get; set; }

        public string Pais { get; set; }       

    }
}