﻿using EscuelaNet.Aplicacion.Clientes.QueryModels;
using EscuelaNet.Dominio.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Clientes.Web.Models
{
    public class NuevaUnidadSolicitudModel
    {
        public int IDUnidad { get; set; }

        public int IDSolicitud { get; set; }

        public string RazonSocialUnidad { get; set; }

        public string TituloSolicitud { get; set; }

        public List<SolicitudesQueryModel> Solicitudes { get; set; }

        public List<UnidadesQueryModel> Unidades { get; set; }

    }
}