﻿using EscuelaNet.Aplicacion.Clientes.Commands.DireccionCommand;
using EscuelaNet.Aplicacion.Clientes.QueryServices;
using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using EsculaNet.Infraestructura.Clientes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MediatR;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class DireccionesController : Controller
    {        
        private IDireccionesQuery _direccionesQuery;
        private IUnidadesQuery _unidadesQuery;
        private IMediator _mediator;

        public DireccionesController( 
                IDireccionesQuery direccionesQuery,
                IUnidadesQuery unidadesQuery,
                IMediator mediator)
        {            
            _direccionesQuery = direccionesQuery;
            _unidadesQuery = unidadesQuery;
            _mediator = mediator;
        }

        public ActionResult Index(int id)
        {       
            var direcciones = _direccionesQuery.ListDirecciones(id);

            if (direcciones.Count!=0)
            {
                var model = new DireccionesIndexModel()
                {
                    Titulo = "Direcciones de la Unidad '" + direcciones[0].RazonSocialUnidad + "'",
                    Direcciones = direcciones,
                    IdUnidad = id,
                    IdCliente = direcciones[0].IdCliente
                };

                return View(model);
            }
            else
            {
                var unidad = _unidadesQuery.GetUnidadDeNegocio(id);
                TempData["error"] = "Unidad sin direcciones";
                return RedirectToAction("../Unidades/Index/"+unidad.IdCliente);
            }
        }

        public ActionResult New(int id)
        {
            var unidad = _unidadesQuery.GetUnidadDeNegocio(id);
            var model = new NuevaDireccionModel()
            {
                Titulo = "Nueva Direccion para la  Unidad '"+unidad.RazonSocial+"'",
                IdUnidad = id
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> New(NuevaDireccionCommand model)
        {
            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "Direccion Creada";
                return RedirectToAction("Index/" + model.IdUnidad);
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaDireccionModel()
                {
                    Domicilio = model.Domicilio,
                    Localidad=model.Localidad,
                    Provincia=model.Provincia,
                    Pais=model.Pais,
                    IdUnidad = model.IdUnidad,                    
                };
                return View(modelReturn);
            }            
        }
        
        public ActionResult Edit(int id)
        {
            var direccion = _direccionesQuery.GetDireccion(id);
            if (direccion==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("../Clientes/Index");
            }
            else
            {                
                var unidad = _unidadesQuery.GetUnidadDeNegocio(direccion.IdUnidadDeNegocio);
                var model = new NuevaDireccionModel()
                {
                    Titulo="Editar una Direccion  de la Unidad '"+unidad.RazonSocial+"' del Cliente '"+ unidad.RazonSocialCliente+"'",                    
                    IdUnidad = direccion.IdUnidadDeNegocio,
                    IdDireccion = id,
                    Domicilio = direccion.Domicilio,
                    Localidad = direccion.Localidad,
                    Provincia = direccion.Provincia,
                    Pais = direccion.Pais

                };

                return View(model);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditDireccionCommand model)
        {
            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "Direccion Editada";
                return RedirectToAction("Index/" + model.IdUnidad);
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaDireccionModel()
                {
                    Domicilio = model.Domicilio,
                    Localidad = model.Localidad,
                    Provincia = model.Provincia,
                    Pais = model.Pais,
                    IdUnidad = model.IdUnidad,
                    IdDireccion=model.IdDireccion
                };
                return View(modelReturn);
            }
                       
        }

        public ActionResult Delete(int id)
        {
            var direccion = _direccionesQuery.GetDireccion(id);

            if (direccion==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("../Clientes/Index");
            }
            else
            {
                var unidad = _unidadesQuery.GetUnidadDeNegocio(direccion.IdUnidadDeNegocio);
                var model = new NuevaDireccionModel()
                {
                    Titulo = "Borrar una Direccion  de la Unidad '" + unidad.RazonSocial + "' del Cliente '" + unidad.RazonSocialCliente + "'",                   
                    IdUnidad = direccion.IdUnidadDeNegocio,
                    IdDireccion = id,
                    Domicilio = direccion.Domicilio,
                    Localidad = direccion.Localidad,
                    Provincia = direccion.Provincia,
                    Pais = direccion.Pais

                };
                return View(model);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Delete(DeleteDireccionCommand model)
        {
            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "Direccion Borrada";
                return RedirectToAction("Index/" + model.IdUnidad);
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaDireccionModel()
                {
                    Domicilio = model.Domicilio,
                    Localidad = model.Localidad,
                    Provincia = model.Provincia,
                    Pais = model.Pais,
                    IdUnidad = model.IdUnidad,
                    IdDireccion = model.IdDireccion
                };
                return View(modelReturn);
            }           

        }

    }
}