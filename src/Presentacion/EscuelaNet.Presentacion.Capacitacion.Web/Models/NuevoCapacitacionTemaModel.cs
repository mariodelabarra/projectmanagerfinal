﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EscuelaNet.Aplicacion.Capacitaciones.QueryModels;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Models
{
    public class NuevoCapacitacionTemaModel
    {
        public int IDTema { get; set; }
        public int IDCapacitacion { get; set; }
        public string Capacitacion { get; set; }
        public string NombreTema { get; set; }
        public List<TemaQueryModel> Temas { get; set; }
    }
}