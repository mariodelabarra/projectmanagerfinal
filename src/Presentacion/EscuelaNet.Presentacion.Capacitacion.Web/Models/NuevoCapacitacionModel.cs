﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EscuelaNet.Aplicacion.Capacitaciones.QueryModels;
using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Models
{
    public class NuevoCapacitacionModel
    {
        public int Id { get; set; }
        public int IDLugar { get; set; }
        public int Minimo { get; set; }
        public int Maximo { get; set; }
        public int Duracion { get; set; }
        public decimal Precio { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fin { get; set; }
        public EstadoDeCapacitacion Estado { get; set; }

        public List<LugarQueryModel> Lugares { get; set; }
    }
}