﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Aplicacion.Capacitaciones.Commands;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.LugarCommands;
using EscuelaNet.Aplicacion.Capacitaciones.QueryServices;
using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Presentacion.Capacitacion.Web;
using EscuelaNet.Presentacion.Capacitacion.Web.Models;
using MediatR;
using Microsoft.Ajax.Utilities;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Controllers
{
    public class LugaresController : Controller
    {
       
        private ILugarRepository _lugaresRepositorio ;
        private ILugaresQuery _lugaresQuery;
        private IMediator _mediator;
        public LugaresController(ILugarRepository lugaresRepositorio, ILugaresQuery lugaresQuery, IMediator mediator)
        {
            _lugaresRepositorio = lugaresRepositorio;
            _lugaresQuery = lugaresQuery;
            _mediator = mediator;

        }
        // GET: Lugares
        public ActionResult Index()
        {
            var lugares = _lugaresQuery.ListLugares();
            var model = new LugarIndexModel()
            {
                Titulo = "Lugares",
                Lugares = lugares
            };

            return View(model);
        }


        // GET: Lugares/Create
        public ActionResult New()
        {
            var model = new NuevoLugarModel();
            return View(model);
        }

        // POST: Lugares/Create
        [HttpPost]
        public async Task<ActionResult> New(NuevoLugarCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Lugar creado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoLugarModel()
                {
                    Capacidad = model.Capacidad,
                    Calle = model.Calle,
                    Numero = model.Numero,
                    Depto = model.Depto,
                    Piso = model.Piso,
                    Localidad = model.Localidad,
                    Provincia = model.Provincia,
                    Pais = model.Pais

                };
                return View(modelReturn);
            }

        }

        // GET: Lugares/Edit/5
        public ActionResult Edit(int id)
        {
            var lugar = _lugaresQuery.GetLugar(id);

            var model = new NuevoLugarModel()
            {
                Capacidad = lugar.Capacidad,
                Id = id,
                Calle = lugar.Calle,
                Numero = lugar.Numero,
                Depto = lugar.Depto,
                Piso = lugar.Piso,
                Localidad = lugar.Localidad,
                Provincia = lugar.Provincia,
                Pais = lugar.Pais,
            };
            return View(model);
        }

        // POST: Lugares/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(UpdateLugarCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Lugar Editado";
                return RedirectToAction("Index");
            }
            else
            {

                TempData["error"] = exito.Error;
                var modelReturn = new NuevoLugarModel()
                {
                    Capacidad = model.Capacidad,
                    Calle = model.Calle,
                    Numero = model.Numero,
                    Depto = model.Depto,
                    Piso = model.Piso,
                    Localidad = model.Localidad,
                    Provincia = model.Provincia,
                    Pais = model.Pais,
                };
                return View(modelReturn);
            }       
                
        }

        // GET: Lugares/Delete/5
        public ActionResult Delete(int id)
        {
            var lugar = _lugaresQuery.GetLugar(id);

            var model = new NuevoLugarModel()
            {
                Capacidad = lugar.Capacidad,
                Id = id,
                Calle = lugar.Calle,
                Numero = lugar.Numero,
                Depto = lugar.Depto,
                Piso = lugar.Piso,
                Localidad = lugar.Localidad,
                Provincia = lugar.Provincia,
                Pais = lugar.Pais,
            };
            return View(model);
        }

        // POST: Lugares/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(DeleteLugarCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Lugar Elimninado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoLugarModel()
                {
                    Capacidad = model.Capacidad,

                    Calle = model.Calle,
                    Numero = model.Numero,
                    Depto = model.Depto,
                    Piso = model.Piso,
                    Localidad = model.Localidad,
                    Provincia = model.Provincia,
                    Pais = model.Pais,
                };
                return View(modelReturn);
               
            }
        }
    }
}
