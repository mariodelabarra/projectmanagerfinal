﻿using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Capacitacion : Entity, IAggregateRoot
    {
       
        public int IDLugar { get; set; }
       // public Lugar Lugar { get; set; }
        public int Minimo { get; set; }
        public int Maximo { get; set; }
        public int Duracion { get; set; }
        public decimal Precio { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fin { get; set; }
        public EstadoDeCapacitacion Estado { get; set; }
        //public IList<Alumno> Alumnos { get; set; }
        public IList<Tema> Temas { get; set; }

        //public IList<Instructor> Instructores { get; set; }

        public Capacitacion()
        {
            
        }

        public Capacitacion(int idLugar, int minimo,int maximo,int duracion, decimal precio,
                DateTime inicio, DateTime fin, EstadoDeCapacitacion estado)
        {
            this.IDLugar = idLugar;
            this.Minimo = minimo;
            this.Maximo = maximo;
            this.Duracion = duracion;
            this.Precio = precio;
            this.Inicio = inicio;
            this.Fin = fin;
            this.Estado = estado;
        }

        public void SetDuracion(int duracion)
        {
            if (duracion <= 0)
                throw new ExcepcionDeProyectos("Error La Duracion de la Capacitacion no puede ser 0");
            Duracion = duracion;
        }
        public int GetDuracion()
        {
            return Duracion;
        }

        //public void PushAlumno(Alumno Alumno)
        //{
        //    if (this.Alumnos == null)
        //    {
        //        this.Alumnos = new List<Alumno>();
        //    }
        //    this.Alumnos.Add(new Alumno(Alumno.Nombre, Alumno.Apellido, Alumno.Dni,Alumno.FechaNacimiento));
        //}

        //public void AddInstructor(Instructor instructor)
        //{
        //    if (this.Instructores == null)
        //    {
        //        this.Instructores = new List<Instructor>();
        //    }

        //    this.Instructores.Add(instructor);
                        
        //}

        public void PushTema(Tema tema)
        {
            if (this.Temas == null)
            {
                this.Temas = new List<Tema>();
            }
            if (!this.Temas.Contains(tema))
            {
                this.Temas.Add(tema);
            }
            else
                throw new Exception("Ya se encuentra el tema vinculado al Capacitacion.");

            
        }

        public void pullTema(Tema tema)
        {
            if (this.Temas != null)
            {
                if (this.Temas.Contains(tema))
                {
                    this.Temas.Remove(tema);
                }
            }

        }
    }
}
