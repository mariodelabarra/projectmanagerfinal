﻿namespace EscuelaNet.Dominio.Capacitaciones
{
    public enum EstadoDeCapacitacion
    {
        SinIniciar,
        Iniciado,
        Finalizado
        
    }
}