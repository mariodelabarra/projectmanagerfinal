﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public interface ICapacitacionRepository : IRepository<Capacitacion>
    {
        Capacitacion Add(Capacitacion capacitacion);
        void Update(Capacitacion capacitacion);
        void Delete(Capacitacion capacitacion);
        Capacitacion GetCapacitacion(int id);
        List<Capacitacion> ListCapacitaciones();
    }
}
