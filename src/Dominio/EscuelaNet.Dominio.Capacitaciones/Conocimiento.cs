﻿using System.Security.Cryptography;
using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Conocimiento : Entity
    {
       
        public Conocimiento()
        {

        }

        public Conocimiento(Conocimiento conocimiento)
        { 
           
            Descripcion = conocimiento.Descripcion;
            Nivel = conocimiento.Nivel;

        }

        public string Descripcion { get; set; }
        public int Nivel { get; set; }

    }
}