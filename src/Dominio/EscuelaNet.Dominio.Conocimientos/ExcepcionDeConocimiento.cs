﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
    public class ExcepcionDeConocimiento : Exception
    {
        public ExcepcionDeConocimiento(string message) : base(message)
        { }

    }
}
