using EscuelaNet.Dominio.SeedWoork;
using System.Collections.Generic;
using System.Linq;

namespace EscuelaNet.Dominio.Clientes
{
    public class UnidadDeNegocio : Entity
    {
        public string RazonSocial { get; set; }

        public string ResponsableDeUnidad { get; set; }

        public string Cuit { get; set; }

        public string EmailResponsable { get; set; }

        public string TelefonoResponsable { get; set; }

        public IList<Direccion> Direcciones { get; private set; }

        public IList<Solicitud> Solicitudes { get; set; }

        public Cliente Cliente { get; set; }

        public int IDCliente { get; set; }

        public string Humor { get; set; }

        public bool AceptarProyecto { get; set; }

        private UnidadDeNegocio() {
            
        }

        public UnidadDeNegocio(string razonSocial, 
            string responsable, string cuit, string email, 
            string telefono)
        {
            this.RazonSocial = razonSocial ?? throw new System.ArgumentNullException(nameof(razonSocial));
            this.ResponsableDeUnidad = responsable ?? throw new System.ArgumentNullException(nameof(responsable));
            this.Cuit = cuit ?? throw new System.ArgumentNullException(nameof(cuit));
            this.EmailResponsable = email ?? throw new System.ArgumentNullException(nameof(email));
            this.TelefonoResponsable = telefono ?? throw new System.ArgumentNullException(nameof(telefono));
            this.CalcularHumor();
        }

        public void AgregarDireccion(Direccion direccion)
        {
            if (this.Direcciones == null)
            {
                this.Direcciones = new List<Direccion>();
            }
            if (!this.Direcciones.Any(x => x.toString() == direccion.toString()))
            {
                this.Direcciones.Add(direccion);
            }
        }

        public void CalcularHumor()
        {
            var cantidadSolicitudesTotal = 0;
            var cantidadSolicitudesBD = 0;

            if (this.Solicitudes != null)
            {
                foreach (var solicitud in this.Solicitudes)
                {
                    if (solicitud.Estado != EstadoSolicitud.Abandonado)
                    {
                        cantidadSolicitudesTotal++;

                        if (solicitud.Estado == EstadoSolicitud.Borrador
                            || solicitud.Estado == EstadoSolicitud.Desarrollo)
                        {
                            cantidadSolicitudesBD++;
                        }
                    }
                }
            }

            if (cantidadSolicitudesTotal == 0)
            {
                this.Humor = "Nueva Unidad - Indefinido";
                this.AceptarProyecto = true;
            }
            else
            {
                var humor = 100 * (cantidadSolicitudesTotal - cantidadSolicitudesBD) / cantidadSolicitudesTotal;

                if (humor < 20 && this.Solicitudes.Count() >3)
                {
                    this.Humor = "Mal Humor";
                    this.AceptarProyecto = false;
                }
                else if (humor>= 20 && humor < 60)
                {
                    this.Humor = "Humor bajo";
                    this.AceptarProyecto = true;
                }
                else
                {
                    this.Humor = "Buen Humor";
                    this.AceptarProyecto = true;
                }
            }
        }    

        public void AgregarSolicitud(Solicitud solicitud)
        {
            if (this.Solicitudes == null)
            {
                this.Solicitudes = new List<Solicitud>();
            }
            if (!this.Solicitudes.Contains(solicitud))
            {
                var bandera = true;
                foreach (var unidad in solicitud.UnidadesDeNegocio)
                {
                    if (unidad.IDCliente != this.IDCliente)
                    {
                        bandera = false;
                    }
                }
                if (bandera)
                {
                    if (!this.AceptarProyecto)
                    {
                        throw new ExcepcionDeCliente("Humor de la unidad demasiado bajo para poder vincular una nueva solicitud");
                    }
                    else
                    {
                        this.Solicitudes.Add(solicitud);
                    }
                }
                else
                {
                    throw new ExcepcionDeCliente("No se puede vincular esta solicitud, ya esta vinculada a otro cliente");
                }

            }
            else
            {
                throw new ExcepcionDeCliente("Solicitud ya vinculada a esta Unidad");
            }

            this.CalcularHumor();
        }

        public void PullSolicitud(Solicitud solicitud)
        {
            if (this.Solicitudes != null)
            {
                if (this.Solicitudes.Contains(solicitud))
                {
                    this.Solicitudes.Remove(solicitud);
                }
            }
            this.CalcularHumor();
        }

    }
}
