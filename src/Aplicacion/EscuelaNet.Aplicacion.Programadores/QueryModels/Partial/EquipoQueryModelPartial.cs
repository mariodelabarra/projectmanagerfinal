﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace EscuelaNet.Aplicacion.Programadores.QueryModels
{
    public partial class EquipoQueryModel
    {
        public string DisableBotonProgramadores
        {
            get
            {
                return this.Total.GetValueOrDefault() <= 0 ? "disabled" :"";
            }
        }
    }
}
