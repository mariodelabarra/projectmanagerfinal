﻿using EscuelaNet.Aplicacion.Conocimiento.Responds;
using EscuelaNet.Dominio.Conocimientos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.Command.ConocimientoCommand
{
    public class DeleteConocimientoCommand : IRequest<CommandRespond>
    {
        public int IdConocimiento { get; set; }

        public int IdCategoria { get; set; }

        public string Nombre { get; set; }
        public Demanda Demanda { get; set; }

        public string NombreConocimiento { get; set; }
    }
}
