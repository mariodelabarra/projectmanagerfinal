﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.QueryModels
{
    public class ConocimientoQueryModel
    {
        public string Nombre { get; set; }
        public Demanda Demanda { get; set; }
        public int IDCategoria { get; set; }
        public int ID { get; set; }
        public string NombreCategoria { get; set; }
    }
}
