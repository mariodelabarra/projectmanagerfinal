﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.QueryModels
{
    public class AsesorQueryModel
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Idioma { get; set; }
        public string Pais { get; set; }
        public Disponibilidad Disponibilidad { get; set; }
    }
}
