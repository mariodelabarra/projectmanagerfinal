﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EscuelaNet.Aplicacion.Conocimiento.Responds;
using EscuelaNet.Aplicacion.Conocimientos.Commands.CategoriaCommand;
using EscuelaNet.Dominio.Conocimientos;
using MediatR;

namespace EscuelaNet.Aplicacion.Conocimiento.CommandsHandlers.CategoriaCommandHandler
{
    public class EditCategoriaCommandHanlders : 
        IRequestHandler<EditCategoriaCommand, CommandRespond>
    {
        private ICategoriaRepository _categoriaRepository;

        public EditCategoriaCommandHanlders(ICategoriaRepository categoriaRepository)
        {
            _categoriaRepository = categoriaRepository;
        }
        public Task<CommandRespond> Handle(EditCategoriaCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            if (!string.IsNullOrEmpty(request.Nombre))
            {
                try
                {
                    var categoria = _categoriaRepository.GetCategoria(request.Id);
                    categoria.Nombre = request.Nombre;
                    categoria.Descripcion = request.Descripcion;

                    _categoriaRepository.Update(categoria);
                    _categoriaRepository.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);
                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "El nombre está vacio.";
                return Task.FromResult(responde);
            }
        }
    }
}
