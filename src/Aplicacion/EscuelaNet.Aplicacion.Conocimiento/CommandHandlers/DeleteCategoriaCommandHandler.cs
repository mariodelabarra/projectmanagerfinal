﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EscuelaNet.Aplicacion.Conocimiento.Commands.CategoriaCommand;
using EscuelaNet.Aplicacion.Conocimiento.Responds;
using EscuelaNet.Dominio.Conocimientos;
using MediatR;

namespace EscuelaNet.Aplicacion.Conocimiento.CommandsHandlers.CategoriaCommandHandler
{
    public class DeleteCategoriaCommandHandler : IRequestHandler<DeleteCategoriaCommand, CommandRespond>
    {
        private ICategoriaRepository _temaRepository;

        public DeleteCategoriaCommandHandler(ICategoriaRepository temaRepository)
        {
            _temaRepository = temaRepository;
        }

        public Task<CommandRespond> Handle(DeleteCategoriaCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            try
            {
                var tema = _temaRepository.GetCategoria(request.Id);
                _temaRepository.Delete(tema);
                _temaRepository.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);
            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);
            }
        }
    }
}
