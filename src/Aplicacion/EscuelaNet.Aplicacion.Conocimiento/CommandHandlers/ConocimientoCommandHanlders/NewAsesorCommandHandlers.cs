﻿using EscuelaNet.Aplicacion.Conocimiento.Command.ConocimientoCommand;
using EscuelaNet.Aplicacion.Conocimiento.Responds;
using EscuelaNet.Dominio.Conocimientos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.CommandHandlers.ConocimientoCommandHanlders
{
    public class NewAsesorCommandHandlers : IRequestHandler<NewAsesorCommand, CommandRespond>
    {
        private ICategoriaRepository _categoriarepositorio;
        private IAsesorRepository _asesorepositorio;
        public NewAsesorCommandHandlers(ICategoriaRepository categoriarepositorio, IAsesorRepository asesorepositorio)
        {
            _categoriarepositorio = categoriarepositorio;
            _asesorepositorio = asesorepositorio;
        }

        public Task<CommandRespond> Handle(NewAsesorCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            try
            {
                var conocimientoBuscado = _categoriarepositorio.GetConocimieto(request.IDConocimiento);
                var asesorBuscado = _asesorepositorio.GetAsesor(request.IDAsesor);

                conocimientoBuscado.AgregarAsesor(asesorBuscado);

                _categoriarepositorio.Update(conocimientoBuscado.Categoria);
                _categoriarepositorio.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);

            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);

            }
        }
    }
}
