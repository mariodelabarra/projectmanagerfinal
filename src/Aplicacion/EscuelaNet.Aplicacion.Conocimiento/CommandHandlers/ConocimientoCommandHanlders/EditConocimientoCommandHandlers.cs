﻿using EscuelaNet.Aplicacion.Conocimiento.Command.ConocimientoCommand;
using EscuelaNet.Aplicacion.Conocimiento.Responds;
using EscuelaNet.Dominio.Conocimientos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.CommandHandlers.ConocimientoCommandHanlders
{
    public class EditConocimientoCommandHandlers : IRequestHandler<EditConocimientoCommand, CommandRespond>
    {
        private ICategoriaRepository _categoriarepositorio;
        public EditConocimientoCommandHandlers(ICategoriaRepository categoriarepositorio)
        {
            _categoriarepositorio = categoriarepositorio;
        }

        public Task<CommandRespond> Handle(EditConocimientoCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            if (!string.IsNullOrEmpty(request.Nombre))
            {
                try
                {
                    var categoria = _categoriarepositorio.GetCategoria(request.IdCate);
                    categoria.Conocimientos.Where(un => un.ID == request.IdConocimiento).First().Nombre = request.Nombre;
                    categoria.Conocimientos.Where(un => un.ID == request.IdConocimiento).First().Demanda = request.Demanda;

                    _categoriarepositorio.Update(categoria);
                    _categoriarepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);
                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "Complete todos los campos.";
                return Task.FromResult(responde);
            }
        }
    }

}
