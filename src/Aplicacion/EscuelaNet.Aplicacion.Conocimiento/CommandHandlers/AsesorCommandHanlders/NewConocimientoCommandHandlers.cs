﻿using EscuelaNet.Aplicacion.Conocimiento.Command.AsesorCommand;
using EscuelaNet.Aplicacion.Conocimiento.Responds;
using EscuelaNet.Dominio.Conocimientos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.CommandHandlers.AsesorCommandHanlders
{
    public class NewConocimientoCommandHandlers : IRequestHandler<NewConocimientoCommand, CommandRespond>
    {
        private ICategoriaRepository _categoriarepositorio;
        private IAsesorRepository _asesorrepositorio;

        public NewConocimientoCommandHandlers(ICategoriaRepository categoriarepositorio, IAsesorRepository asesorrepositorio)
        {
            _categoriarepositorio = categoriarepositorio;
            _asesorrepositorio = asesorrepositorio;
        }
        public Task<CommandRespond> Handle(NewConocimientoCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            try
            {
                var asesorbuscado = _asesorrepositorio.GetAsesor(request.IDAsesor);
                var conocimientobuscado = _categoriarepositorio.GetConocimieto(request.IDConocimiento);

                conocimientobuscado.AgregarAsesor(asesorbuscado);
                _categoriarepositorio.Update(conocimientobuscado.Categoria);
                _categoriarepositorio.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);

            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);
            }
        }
    }
}
