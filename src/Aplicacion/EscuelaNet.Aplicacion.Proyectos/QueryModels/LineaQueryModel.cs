﻿using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Proyectos.QueryModels
{
    public class LineaQueryModel
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public List<Proyecto> Proyectos { get; set; }
    }
}
