﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Aplicacion.Capacitaciones.QueryModels
{
    public class InstructorQueryModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Dni { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public List<Tema> Temas { get; set; }

    }
}
