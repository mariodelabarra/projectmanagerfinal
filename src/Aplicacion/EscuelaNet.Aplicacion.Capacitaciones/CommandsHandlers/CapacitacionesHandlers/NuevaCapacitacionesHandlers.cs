﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.CapacitacionCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.InstructorCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers
{
    public class NuevaCapacitacionesHandlers : IRequestHandler<NuevoCapacitacionCommand, CommandRespond>
    {
        private ICapacitacionRepository _capacitacionRepository;

        public NuevaCapacitacionesHandlers(ICapacitacionRepository capacitacionRepository)
        {
            _capacitacionRepository = capacitacionRepository;
        }

        public Task<CommandRespond> Handle(NuevoCapacitacionCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            if (request.Duracion > 0 && request.Minimo > 0 && request.Maximo > 0
                && request.Precio >= 0)
            {
                _capacitacionRepository.Add(new Capacitacion(request.IDLugar, request.Minimo, request.Maximo,
                    request.Duracion
                    , request.Precio, request.Inicio, request.Fin, request.Estado));

                _capacitacionRepository.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);

            }
            else
            {
                responde.Succes = false;
                responde.Error = "Error Campos en 0";
                return Task.FromResult(responde);
            }
        }
    }
}

