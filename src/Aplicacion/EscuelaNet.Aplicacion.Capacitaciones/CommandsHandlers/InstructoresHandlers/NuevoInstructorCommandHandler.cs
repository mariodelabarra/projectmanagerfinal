﻿using EscuelaNet.Aplicacion.Capacitaciones.Commands.InstructorCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers.InstructoresHandlers
{
    public class NuevoInstructorCommandHandler : IRequestHandler<NuevoInstructorCommand, CommandRespond>
    {
        private IInstructorRepository _instructorRepository;
        public NuevoInstructorCommandHandler(IInstructorRepository instructorRepository)
        {
            _instructorRepository = instructorRepository;
        }

        public Task<CommandRespond> Handle(NuevoInstructorCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            if (string.IsNullOrEmpty(request.Nombre)
               || string.IsNullOrEmpty(request.Apellido)
               || string.IsNullOrEmpty(request.Dni))
            {

                responde.Succes = false;
                responde.Error = "Error Campos Vacios";
                return Task.FromResult(responde);
            }
            else
            {
               
                    _instructorRepository.Add(new Instructor(request.Nombre, request.Apellido,
                        request.Dni, request.FechaNacimiento));

                    _instructorRepository.UnitOfWork.SaveChanges();
                    
                    responde.Succes = true;
                    return Task.FromResult(responde);
              
            }                 
        }
    }
}
