﻿using EscuelaNet.Aplicacion.Capacitaciones.Commands;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.LugarCommands;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers.LugaresHandlers
{
    public class DeleteLugarCommandHandler : IRequestHandler<DeleteLugarCommand, CommandRespond>
    {
        private ILugarRepository _lugarRepository;
        public DeleteLugarCommandHandler(ILugarRepository lugarRepository)
        {
            _lugarRepository = lugarRepository;
        }

        public Task<CommandRespond> Handle(DeleteLugarCommand request, CancellationToken cancellationToken)
        {
            var response = new CommandRespond();
            var tema = _lugarRepository.GetLugar(request.Id);
            _lugarRepository.Delete(tema);
            _lugarRepository.UnitOfWork.SaveChanges();
            response.Succes = true;
            return Task.FromResult(response);

        }
    }
}
