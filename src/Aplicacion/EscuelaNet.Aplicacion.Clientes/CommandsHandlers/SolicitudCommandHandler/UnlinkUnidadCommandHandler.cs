﻿using EscuelaNet.Aplicacion.Clientes.Commands.SolicitudCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.SolicitudCommandHandler
{
    public class UnlinkUnidadCommandHandler :
        IRequestHandler<UnlinkUnidadCommand, CommandRespond>
    {
        private IClienteRepository _clientesRepositorio;
        private ISolicitudRepository _solicitudesRepositorio;

        public UnlinkUnidadCommandHandler(IClienteRepository clientesRepositorio,
            ISolicitudRepository solicitudRepository)
        {
            _clientesRepositorio = clientesRepositorio;
            _solicitudesRepositorio = solicitudRepository;
        }

        public Task<CommandRespond> Handle(UnlinkUnidadCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            try
            {
                var solicitudBuscada = _solicitudesRepositorio.GetSolicitud(request.IDSolicitud);
                var unidadBuscada = _clientesRepositorio.GetUnidadDeNegocio(request.IDUnidad);
                unidadBuscada.PullSolicitud(solicitudBuscada);

                _solicitudesRepositorio.Update(solicitudBuscada);
                _solicitudesRepositorio.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);

            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);
            }
        }
    }
}
