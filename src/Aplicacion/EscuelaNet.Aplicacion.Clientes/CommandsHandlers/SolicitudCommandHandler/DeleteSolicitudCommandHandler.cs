﻿using EscuelaNet.Aplicacion.Clientes.Commands.SolicitudCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.SolicitudCommandHandler
{
    public class DeleteSolicitudCommandHandler :
        IRequestHandler<DeleteSolicitudCommand, CommandRespond>
    {
        private ISolicitudRepository _solicitudesRepositorio;
        private IClienteRepository _clienteRepository;

        public DeleteSolicitudCommandHandler(ISolicitudRepository solicitudesRepositorio, 
            IClienteRepository clienteRepository)
        {
            _solicitudesRepositorio = solicitudesRepositorio;
            _clienteRepository = clienteRepository;
        }
       

        public Task<CommandRespond> Handle(DeleteSolicitudCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            try
            {
                var solicitud = _solicitudesRepositorio.GetSolicitud(request.IdSolicitud);

                List<int> listaId = new List<int>();

                foreach (var unidad in solicitud.UnidadesDeNegocio)
                {
                    var id = unidad.ID;
                    listaId.Add(id);
                }

                _solicitudesRepositorio.Delete(solicitud);
                _solicitudesRepositorio.UnitOfWork.SaveChanges();

                foreach (var idUnidad in listaId)
                {
                    var unidadBuscada = _clienteRepository.GetUnidadDeNegocio(idUnidad);
                    unidadBuscada.CalcularHumor();
                    _clienteRepository.Update(unidadBuscada.Cliente);
                    _clienteRepository.UnitOfWork.SaveChanges();
                }
                
                responde.Succes = true;
                return Task.FromResult(responde);

            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);
            }
        }
    }
}
