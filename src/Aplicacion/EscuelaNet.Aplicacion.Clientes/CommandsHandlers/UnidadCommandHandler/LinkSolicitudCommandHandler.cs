﻿using EscuelaNet.Aplicacion.Clientes.Commands.UnidadCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.UnidadCommandHandler
{
    class LinkSolicitudCommandHandler :
        IRequestHandler<LinkSolicitudCommand, CommandRespond>
    {
        private IClienteRepository _clientesRepositorio;
        private ISolicitudRepository _solicitudesRepositorio;

        public LinkSolicitudCommandHandler(IClienteRepository clientesRepositorio,
            ISolicitudRepository solicitudRepository)
        {
            _clientesRepositorio = clientesRepositorio;
            _solicitudesRepositorio = solicitudRepository;
        }
               
        public Task<CommandRespond> Handle(LinkSolicitudCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            try
            {
                var unidadBuscada = _clientesRepositorio.GetUnidadDeNegocio(request.IDUnidad);
                var solicitudBuscada = _solicitudesRepositorio.GetSolicitud(request.IDSolicitud);

                 unidadBuscada.AgregarSolicitud(solicitudBuscada);

                _clientesRepositorio.Update(unidadBuscada.Cliente);
                _clientesRepositorio.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);

            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);
            }   
        }
    }
}
