﻿using EscuelaNet.Aplicacion.Clientes.Commands.DireccionCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.DireccionCommandHandler
{
    public class DeleteDireccionCommandHandler :
        IRequestHandler<DeleteDireccionCommand, CommandRespond>
    {
        private IClienteRepository _clienteRepositorio;

        public DeleteDireccionCommandHandler(IClienteRepository clienteRepositorio)
        {
            _clienteRepositorio = clienteRepositorio;
        }

        public Task<CommandRespond> Handle(DeleteDireccionCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            try
            {
                var direccion = _clienteRepositorio.GetDireccion(request.IdDireccion);
                _clienteRepositorio.DeleteDireccion(direccion);
                _clienteRepositorio.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);
            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);
            }
        }
    }
}
