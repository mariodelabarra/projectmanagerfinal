﻿using EscuelaNet.Aplicacion.Clientes.Commands.ClienteCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.ClienteCommandHandler
{
    public class EditClienteCommandHandler :
        IRequestHandler<EditClienteCommand, CommandRespond>
    {
        private IClienteRepository _clienteRepositorio;

        public EditClienteCommandHandler(IClienteRepository clienteRepositorio)
        {
            _clienteRepositorio = clienteRepositorio;
        }

        public Task<CommandRespond> Handle(EditClienteCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            if (!string.IsNullOrEmpty(request.RazonSocial))
            {
                try
                {
                    var cliente = _clienteRepositorio.GetCliente(request.Id);
                    cliente.RazonSocial = request.RazonSocial;
                    cliente.Email = request.Email;
                    cliente.Categoria = request.Categoria;

                    _clienteRepositorio.Update(cliente);
                    _clienteRepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);
                }

            }
            else
            {
                responde.Succes = false;
                responde.Error = "La Razon Social está vacia.";
                return Task.FromResult(responde);
            }


        }
    }
}
