﻿using EscuelaNet.Dominio.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.QueryModels
{
    public class ClientesQueryModel
    {
        public int ID { get; set; }

        public string RazonSocial { get; set; }

        public string Email { get; set; }

        public Categoria Categoria { get; set; }


    }
}
